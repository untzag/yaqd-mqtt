# yaqd-mqtt

[![PyPI](https://img.shields.io/pypi/v/yaqd-mqtt)](https://pypi.org/project/yaqd-mqtt)
[![Conda](https://img.shields.io/conda/vn/conda-forge/yaqd-mqtt)](https://anaconda.org/conda-forge/yaqd-mqtt)
[![yaq](https://img.shields.io/badge/framework-yaq-orange)](https://yaq.fyi/)
[![black](https://img.shields.io/badge/code--style-black-black)](https://black.readthedocs.io/)
[![ver](https://img.shields.io/badge/calver-YYYY.0M.MICRO-blue)](https://calver.org/)
[![log](https://img.shields.io/badge/change-log-informational)](https://gitlab.com/yaq/yaqd-mqtt/-/blob/master/CHANGELOG.md)

yaq daemons for interacting with MQTT brokers

This package contains the following daemon(s):

- https://yaq.fyi/daemons/my-daemon